function loadCategories(db){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/assets/data/categories.json', true);

    xhr.onload = function(){
        if(this.status == 200){
            var categories = JSON.parse(this.responseText);
            var catx = db.transaction(['category'], 'readwrite');
            var store = catx.objectStore('category');
            // console.log(store);
            for(var i = 0; i < categories.length; i++){
                var request = store.add(categories[i]);

                request.onsuccess = function(evt){
                    console.log("Category successfully loaded!");
                }

                request.onerror = function(evt){
                    console.log("Error occured: " + evt.target.error.name);
                }
            }
        }
    }

    xhr.send();
}

function loadProducts(db){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/assets/data/products.json', true);

    xhr.onload = function(){
        if(this.status == 200){
            var products = JSON.parse(this.responseText);
            var prodtx = db.transaction(['product'], 'readwrite');
            var store = prodtx.objectStore('product');
            for(var i = 0; i < products.length; i++){
                var request = store.add(products[i]);

                request.onsuccess = function(evt){
                    console.log("Product successfully loaded!");
                }

                request.onerror = function(evt){
                    console.log("Error occured: " + evt.target.error.name);
                }
            }
        }
    }

    xhr.send();
}

function loadUsers(db){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/assets/data/users.json', true);

    xhr.onload = function(){
        if(this.status == 200){
            var users = JSON.parse(this.responseText);
            var usertx = db.transaction(['user'], 'readwrite');
            var store = usertx.objectStore('user');
            for(var i = 0; i < users.length; i++){
                var request = store.add(users[i]);

                request.onsuccess = function(evt){
                    console.log("User successfully loaded!");
                }

                request.onerror = function(evt){
                    console.log("Error occured: " + evt.target.error.name);
                }
            }
        }
    }

    xhr.send();
}

function addToCarousel(db, carouselElm){
    var transaction = db.transaction(['category'], 'readonly');
    var store = transaction.objectStore('category');

    store.openCursor().onsuccess = function(evt){
        var cursor = evt.target.result;
        if(cursor){
            var name = cursor.value.category_name;
            var image = cursor.value.category_name.toString().toLowerCase().split(" ").join("-") + ".jpg";
            carouselElm.append(`<img class="item" style="max-width: 100%; height: auto;" src="assets/images/categories/${image}" data-dot="${name}">`);
            cursor.continue();
        } else{
            carouselElm.owlCarousel({
                items: 1,
                loop: true,
                dots: true,
                autoplay: true,
                autoplayTimeout: 7000,
                responsive: {
                    0: { dotsData: false },
                    960: { dotsData: true }
                }
            });
        }
    }
}

function showProducts(db, container){
    var transaction = db.transaction(['product'], 'readonly');
    var store = transaction.objectStore('product');

    store.openCursor().onsuccess = function(evt){
        var cursor = evt.target.result;
        if(cursor){
            container.append(`
                <div class="prod-card">
                    <img class="prod-card__image" src="assets/images/products/${cursor.value.productid}.jpg">
                    <div class="prod-card__det">
                        <a class="prod-name" href="#" title="${cursor.value.product_name}">${cursor.value.product_name}</a>
                        <div class="prod-extra">
                            <span class="price">GH&cent;${cursor.value.price}</span>
                            <button class="btn-cart" data-prodid="${cursor.value.productid}" data-prodqty="1">&#x1f6d2; Add to Cart</button>
                        </di>
                    </div>
                </div>
            `);

            cursor.continue();
        } else{
            // when there is a click event on dynamically created cart button then add to cart ...
            $('.btn-cart').click(function(){
                var prodid = $(this).data('prodid');
                var qty = $(this).data('prodqty');
        
                addToCart(prodid, qty);
            });
        }
    }
}

function performSearch(db, searchTerm, container){
    var transaction = db.transaction(['product'], 'readonly');
    var store = transaction.objectStore('product');
    container.html("");

    store.openCursor().onsuccess = function(evt){
        var cursor = evt.target.result;
        if(cursor){
            if(cursor.value.product_name.toLowerCase().indexOf(searchTerm) !== -1){
                container.append(`
                    <div class="prod-card">
                        <img class="prod-card__image" src="assets/images/products/${cursor.value.productid}.jpg">
                        <div class="prod-card__det">
                            <a class="prod-name" href="#" title="${cursor.value.product_name}">${cursor.value.product_name}</a>
                            <div class="prod-extra">
                                <span class="price">GH&cent;${cursor.value.price}</span>
                                <button class="btn-cart">&#x1f6d2; Add to Cart</button>
                            </di>
                        </div>
                    </div>
                `);
            }

            cursor.continue();
        } else{
            // when there is a click event on dynamically created cart button then add to cart ...
            $('.btn-cart').click(function(){
                var prodid = $(this).data('prodid');
                var qty = $(this).data('prodqty');
        
                addToCart(prodid, qty);
            });
        }
    }
}

function authentication(db, username, password){
    sessionStorage.clear();
    var transaction = db.transaction(['user'], 'readonly');
    var store = transaction.objectStore('user');
    var idx_name = store.index('username');

    var request = idx_name.get(username);
    request.onsuccess = function(evt){
        var user = request.result;
        var hash = md5(password);
        
        // get p.msg element;
        var msg_element = $('.msg');
        msg_element.hide();

        if(typeof user === 'undefined'){
            msg_element.html("User does not exist. Please register!!!").show();
            return false
        }

        if(hash !== user.password){
            msg_element.html("Wrong username or password!!!").show();
            return false
        }

        if(typeof user !== 'undefined' && hash === user.password){
            delete user.password;
            sessionStorage.setItem('user', JSON.stringify(user));
            window.location.href = "index.html";
        }
    }

    request.onerror = function(evt){
        console.log("Error occured: " + evt.target.error.name);
    }
}

function registration(db, data){
    sessionStorage.clear();
    var transaction = db.transaction(['user'], 'readwrite');
    var store = transaction.objectStore('user');

    // get p.msg element;
    var msg_element = $('.msg');
    msg_element.hide();
    
    var request = store.add(data);
    request.onsuccess = function(evt){
        delete data.password;
        sessionStorage.setItem('user', JSON.stringify(data));
        console.log('User registered successfully!');
        window.location.href = "index.html";
    }

    request.onerror = function(evt){
        console.log("Error occured: ", evt.target.error.name);
        if(evt.target.error.name === "ConstraintError"){
            msg_element.html("Email/Username/Cellphone already exists! Try logging in or create a new account altogether!!").show();
        }
        return false;
    }
}

function logout(){
    if(confirm("Are you done shopping? Are you sure you want to logout? ;-(")){
        sessionStorage.clear();
        window.location.href = "authentication.html";
    }
}

function addToCart(prodid, qty){
    var userObject = JSON.parse(sessionStorage.getItem('user'));
    if(userObject === null){
        if(confirm("You want to add item to cart. Do you want to login?")){
            window.location.href = "authentication.html";
        } else{
            alert("You need to login before you can add to cart!!!");
        }
    } else{
        var cart = userObject.cart || Array();
        var found = false;
        var cartItem = {
            product: prodid,
            quantity: qty
        }

        if(cart.length === 0){
            found = false;
        }

        for(var c = 0; c < cart.length; c++){
            if(cart[c].product === cartItem.product){
                found = true;
                break;
            }
        }

        if(found === false){
            cart.push(cartItem);
            userObject.cart = cart;
            sessionStorage.setItem('user', JSON.stringify(userObject));
            window.location.reload();
        } else{
            alert("Item is already in your cart. Go to cart to increase the quantity!!");
        }
    }
}
