$(document).ready(function(){
    if(!indexedDB){
        window.alert("Your browser doesn't support a stable version of IndexDB, as a result dynamic interactions cannot be achieved within this webpage!");
    } else{
        var carouselElm = $('#main-carousel');
        var prodContainer = $('#products-container');
        var login = $('#login');
        var register = $('#register');
        var welcome = $('#welcome');
        var cart = $('#cart');

        // Initialize IndexedDB
        var request = indexedDB.open("ProvyShop", 1);

        request.onupgradeneeded = function(evt){
            var db = evt.target.result;
            
            if(!db.objectStoreNames.contains('category')){
                var catos = db.createObjectStore('category', {keyPath: 'categoryid', autoIncrement: true});
                catos.createIndex('category_name', 'category_name', {unique: true});
            }

            if(!db.objectStoreNames.contains('product')){
                var prodos = db.createObjectStore('product', {keyPath: 'productid', autoIncrement: true});
                prodos.createIndex('product_name', 'product_name', {unique: true});
            }

            if(!db.objectStoreNames.contains('user')){
                var useros = db.createObjectStore('user', {keyPath: 'userid', autoIncrement: true});
                useros.createIndex('username', 'username', {unique: true});
                useros.createIndex('email', 'email', {unique: true});
                useros.createIndex('cellphone', 'cellphone', {unique: true});

                useros.transaction.oncomplete = function(){ 
                    loadProducts(db);
                    loadUsers(db); 
                    loadCategories(db);
                }
            }
        }

        request.onsuccess = function(evt){
            console.log("Successful!");
            var db = evt.target.result;
            if(carouselElm.length === 1) addToCarousel(db, carouselElm);
            if(prodContainer.length === 1) showProducts(db, prodContainer);

            var searchForm = document.getElementById('search-form');
            if(searchForm){
                searchForm.onsubmit = function(){
                    var searchTerm = document.getElementById('search-term').value.toLowerCase();
                    performSearch(db, searchTerm, prodContainer);
                    return false;
                }
            }

            if(login){
                login.submit(function(){
                    var username = $('#username').val();
                    var password = $('#password').val();
                    authentication(db, username, password);
                    return false;
                });
            }

            if(register){
                register.submit(function(){
                    if($('#new_password').val() !== $('#cpassword').val()){
                        $('.msg').html("Passwords do not match!").show();
                    } else{
                        var data = {
                            first_name: $('#firstname').val(),
                            last_name: $('#lastname').val(),
                            cellphone: $('#cellphone').val(),
                            email: $('#email').val(),
                            username: $('#new_username').val(),
                            role: "user",
                            password: md5($('#new_password').val())
                        }

                        registration(db, data);
                    }

                    return false;
                });
            }
        }
 
        request.onerror = function(){
            console.log("Error! Could not open database ...");
        }

        // Authentication functionality ...
        register.hide();

        $('#register-link').click(function(){
            login.hide();
            register.show();
        });

        $('#login-link').click(function(){
            register.hide();
            login.show();
        });

        var sessionData = JSON.parse(sessionStorage.getItem('user'));
        if(sessionData !== null){
            welcome.addClass('dropdown');
            welcome.html(`
                <a href="javascript:void(0);" class="dropbtn">Welcome ${sessionData.username}</a>
                <div class="dropdown-content">
                    <a href="#">Change Password</a>
                    <a href="javascript:void(0);" onclick="logout();">Logout</a>
                </div>
            `);

            if(typeof sessionData.cart !== 'undefined'){
                cart.html(`Cart (${sessionData.cart.length} items)`);
            } else{
                cart.html("Cart (0 items)");
            }
        } else{
            welcome.html(`<a href="authentication.html">Login / Register</a>`);
            cart.html("Cart (0 items)");
        }
    }
});
