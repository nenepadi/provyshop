PROVY SHOP
==========
Creating a client only e-commerce application with IndexedDB

HOW TO RUN IT
=============
Because of the capabilities of IndexedDB, this should be run from a server envronment.

##### Using an installed web server like apache ...
1. You need to download a webserver. WAMP or XAMPP for Windows will be a good option.
2. Copy the files to the server's document root and with any browser go to ```localhost/<path_to_project>/index.html```

##### Using chrome
1. Install the chrome extension [here](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb/related).
2. After installing launch the app from withn your chrome browser and select the folder where the project files are located.
3. Visit the URL that the chrome extension gives you.

FEATURES
========
- User authentication
- User can add to cart
- Products are stored and retrieved directly in the browser using IndexDB.
- User passwords are hashed with md5 encryption.
- You cannot register multiple users with the same username or email.